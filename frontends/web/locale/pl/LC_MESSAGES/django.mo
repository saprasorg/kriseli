��    "      ,  /   <      �     �     �  	   
               ,     G  -   S     �     �     �     �  /   �  9     F   ;  1   �  >   �     �       0        C     R     p     �     �     �  )   �  #   �          !     .     A     N  �  [  	   �     �        	             *     H  T   ]     �     �  )   �     �  4   	  >   K	  O   �	  6   �	  G   
     Y
     w
  ,   �
     �
  (   �
               <  '   Y  !   �  %   �     �     �     �     �                                    	                 "      !                    
                                                                                    All DICT server: Database: Default Define %(term)s Define (I'm feeling lucky) First match Found one definition Found %(rc)s definitions From Last match results Match everything (experimental) Match headwords exactly Match headwords within Levenshtein distance one Match headwords within given Damerau-Levenshtein distance Match headwords within given Damerau-Levenshtein distance (normalized) Match headwords within given Levenshtein distance Match headwords within given Levenshtein distance (normalized) Match prefixes Match strategy: Match substring occurring anywhere in a headword Match suffixes Match using SOUNDEX algorithm Match word prefixes Match word suffixes Match words exactly Old (basic) regular expressions POSIX 1003.2 (modern) regular expressions Reverse search in Quechua databases Search Search term: last match results less options more options Project-Id-Version: dico 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-11-01 19:11+0100
Last-Translator: Wojciech Polak <polak@gnu.org>
Language-Team: Polish <translation-team-pl@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wszystkie Server DICT: Baza danych: Domyślna Zdefiniuj %(term)s Zdefiniuj (szczęśliwy traf) Pierwsze dopasowanie Znaleziono jedną definicję Znaleziono %(rc)s definicji Znaleziono %(rc)s definicji Z Ostatnio znalezione wyniki Dopasowanie wszystkiego (eksperymentalne) Dokładne dopasowanie słów Dopasowanie lematu w odległości Levenshteina jeden Dopasowanie lematu w zadanej odległości Damerau-Levenshteina Dopasowanie lematu w zadanej odległości Damerau-Levenshteina (znormalizowane) Dopasowanie lematu w zadanej odległości Levenshteina Dopasowanie lematu w zadanej odległości Levenshteina (znormalizowane) Dopasowanie prefiksów słów Strategia dopasowania: Dopasowanie ciągu w dowolnym miejscu lematu Dopasowanie sufiksów słów Dopasowanie za pomocą algorytmu SOUNDEX Dopasowanie prefiksów słów Dopasowanie sufiksów słów Dokładne dopasowanie słów Stare (podstawowe) wyrażenia regularne Wyrażenia regularne POSIX 1003.2 Odwrotne poszukiwanie w bazie Quechua Szukaj Szukana fraza: Ostatnio znalezione wyniki mniej opcji więcej opcji 